﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.Windows.Forms;

namespace Árboles_Binarios
{
    class Arbol<T>
    {
        private Nodo<T> Root;

        private int AnchoLienzo;
        private int AlturaNivel;

        public Arbol()
        {
            Root = null;

            // Información sobre dibujar
            AlturaNivel = 60;
            AnchoLienzo = 540;
        }

       
        public void AñadirRecursivo(T value) //Es un wrapper, envuelve
        {
            Root = W_InsercionRecursiva(value, Root);
        }

        private Nodo<T> W_InsercionRecursiva(T value, Nodo<T> nuevo)
        {

            if (nuevo == null)
            {
                nuevo = new Nodo<T>(value);
                Root = nuevo;
            }
            else
            {

                if (value.GetHashCode() >= nuevo.Value.GetHashCode())
                {

                    nuevo.Right = W_InsercionRecursiva(value, nuevo.Right);
                    nuevo.Right.Padre = nuevo;
                }

                if (value.GetHashCode() < nuevo.Value.GetHashCode())
                {

                    nuevo.Left = W_InsercionRecursiva(value, nuevo.Left);
                    nuevo.Left.Padre = nuevo;
                }
            }
            return nuevo;

        }

        public void Dibujar(Graphics graphics)
        {
            W_Dibujar(graphics, Root, 0, AnchoLienzo, 10);
        }

        public void W_Dibujar(Graphics graphics, Nodo<T> nodo, int margenIzq, int margenDer, int margenSup)
        {
            int mitad = margenIzq + (margenDer - margenIzq)/2; // mitad divide el lienzo por la mitad vertical

            if (nodo == null)
            {
                // Caso base: dibujar nodo nulo
                graphics.DrawLine(new Pen(Color.Black), new Point(mitad - 10, margenSup), new Point(mitad + 10, margenSup));
                graphics.DrawLine(new Pen(Color.Black), new Point(mitad - 5, margenSup + 3), new Point(mitad + 5, margenSup + 3));
            }
            else
            {
                // caso recursivo: dibujar nodo... 
                nodo.Dibujar(graphics, mitad, margenSup);
            
                // ... dibujar sus enlaces...
                graphics.DrawLine(new Pen(Color.Gray), new Point(mitad, margenSup ), new Point((margenIzq + mitad)/2, margenSup + AlturaNivel));
                graphics.DrawLine(new Pen(Color.Gray), new Point(mitad, margenSup ), new Point((mitad + margenDer)/2, margenSup + AlturaNivel));

                // ...y llamar recursivamente para el hijo izquierdo y para el hijo derecho

                W_Dibujar(graphics, nodo.Left, margenIzq, mitad, margenSup + AlturaNivel);
                W_Dibujar(graphics, nodo.Right, mitad, margenDer, margenSup + AlturaNivel);
            }
        }
         

        public Nodo<T> Buscar(T value, ListBox lbExplicacion)
        {
            if (lbExplicacion != null) lbExplicacion.Items.Add("Empiezo a buscar " + value.ToString());
            return W_BuscarRecursivo(Root, value, lbExplicacion);
        }
        private Nodo<T> W_BuscarRecursivo(Nodo<T> node, T value, ListBox lbExplicacion)
        {
            Nodo<T> actual;

            if (node == null)
            {
                if (lbExplicacion != null) lbExplicacion.Items.Add("No encontré " + value.ToString() + " :(");
                actual = null;
            }
            else if (value.Equals(node.Value))
            {
                if (lbExplicacion != null) lbExplicacion.Items.Add("Encontré " + value.ToString() + " :)");
                actual = node;
            }
            else
            {
                if (value.GetHashCode() > node.Value.GetHashCode())
                {
                    if (lbExplicacion != null) lbExplicacion.Items.Add("Veo un " + node.Value.ToString() + ", busco en la derecha");
                    actual = W_BuscarRecursivo(node.Right, value, lbExplicacion);
                }

                else
                {
                    if (lbExplicacion != null) lbExplicacion.Items.Add("Veo un " + node.Value.ToString() + ", busco en la izquierda");
                    actual = W_BuscarRecursivo(node.Left, value, lbExplicacion);
                }


            }
            return actual;

        }
        public Nodo<T> Minimo()
        {
            return W_Minimo(Root);
        }

        private Nodo<T> W_Minimo(Nodo<T> nodo)
        {
            if (nodo.Left == null)
            {
                return nodo;
            }
            else
            {
                nodo = W_Minimo(nodo.Left);
                return nodo;
            }
        }

        public Nodo<T> Maximo()
        {
            return W_Maximo(Root);
        }
        private Nodo<T> W_Maximo(Nodo<T> nodo)
        {
            if (nodo.Right == null)
            {
                return nodo;
            }
            else
            {
                nodo = W_Maximo(nodo.Right);
                return nodo;
            }
        }

        public Nodo<T> Predecesor(T value)
        {
            return W_Predecesor(W_BuscarRecursivo(Root, value, null)); //Permite recorrer el arbol, antes solo se estaba validando la condición sin recorrer el árbol
        }

        private Nodo<T> W_Predecesor(Nodo<T> nodo)
        {


            Nodo<T> actual;


            if (nodo.Left != null)
            {
                actual = W_Maximo(nodo.Left);

            }
            else
            {
                while (nodo != nodo.Padre.Right)
                {
                    nodo = nodo.Padre;

                }
                actual = nodo.Padre;

            }

            return actual;
        }

        public Nodo<T> Sucesor(T value)
        {
            return W_Sucesor(W_BuscarRecursivo(Root, value, null));
        }

        private Nodo<T> W_Sucesor(Nodo<T> nodo)
        {
            //si el valor que se esta metiendo es mayor que el que se esta metiendo a la raiz, y si es menor, nodo. left

            Nodo<T> actual;


            if (nodo.Right != null)
            {
                actual = W_Minimo(nodo.Right);

            }
            else
            {
                while (nodo != nodo.Padre.Left)
                {
                    nodo = nodo.Padre;

                }
                actual = nodo.Padre;

            }

            return actual;
        }
        public void ImprimirInOrden()
        {
            W_ImprimirInOrden(Root);
            Console.WriteLine();
        }
        private void W_ImprimirInOrden(Nodo<T> nodo)
        {
            string str = "";
            str = str + nodo;
            if (nodo != null)
            {

                W_ImprimirInOrden(nodo.Left);
                Console.Write(str + " ");
                W_ImprimirInOrden(nodo.Right);
            }
        }

        public void ImprimirPosOrden()
        {
            W_ImprimirPosOrden(Root);
            Console.WriteLine();
        }
        private void W_ImprimirPosOrden(Nodo<T> nodo)
        {
            string str = "";
            str = str + nodo;
            if (nodo != null)
            {
                W_ImprimirPosOrden(nodo.Right);
                W_ImprimirPosOrden(nodo.Left);
                Console.Write(str + " ");
            }
        }
        public void Transplantar(T nodo, T nodo1)
        {
            W_Transplantar(Buscar(nodo, null), Buscar(nodo1, null));
        }
        public void Espejo()
        {
            W_Espejo(Root);

        }
        private void W_Espejo(Nodo<T> nodo)
        {

            if (nodo.Right != null && nodo.Left != null)
            {
                Nodo<T> mobile;
                mobile = nodo.Left;
                nodo.Left = nodo.Right;
                nodo.Right = mobile;

                //W_Espejo(nodo.Left);
                W_Espejo(nodo.Left);
                W_Espejo(nodo.Right);
            }

        }

        private void W_Transplantar(Nodo<T> nodo, Nodo<T> mobile)
        {
            if (nodo.Padre == null)
            {
                Root = mobile;
            }
            else
            {
                if (nodo == nodo.Padre.Left)
                {
                    nodo.Padre.Left = mobile;
                }
                else
                {
                    nodo.Padre.Right = mobile;
                }
            }

            if (mobile != null)
            {
                mobile.Padre = nodo.Padre;
            }
        }

        public void BorrarArbol(Nodo<T> nodo, ListBox lbExplicacion)
        {
            Nodo<T> y;
            if (nodo.Left == null)
            {
                if (lbExplicacion != null) lbExplicacion.Items.Add("Como el nodo a borrar no tiene hijo izquierdo, transplanto su hijo derecho");
                W_Transplantar(nodo, nodo.Right);
            }
            else if (nodo.Right == null)
            {
                if (lbExplicacion != null) lbExplicacion.Items.Add("Como el nodo a borrar no tiene hijo derecho, transplanto su hijo izquierdo");
                W_Transplantar(nodo, nodo.Left);
            }
            else
            {
                if (lbExplicacion != null) lbExplicacion.Items.Add("Como el nodo a borrar tiene dos hijos, buscaré a su sucesor");
                y = W_Sucesor(nodo);

                if (lbExplicacion != null) lbExplicacion.Items.Add("Resulta que su sucesor es " + y.Value.ToString());
                if (y.Padre != nodo)
                {
                    if (lbExplicacion != null) lbExplicacion.Items.Add("Como el sucesor no tiene como padre al que voy a eliminar, transplanto el hijo derecho del sucesor al lugar del sucesor");
                    W_Transplantar(y, y.Right);
                    y.Right = nodo.Right;
                    y.Right.Padre = y;
                }

                if (lbExplicacion != null) lbExplicacion.Items.Add("Ahora transplanto el sucesor al lugar del nodo a borrar");
                W_Transplantar(nodo, y);
                y.Left = nodo.Left;
                y.Padre = nodo.Padre;
            }
        }
    }
}
