﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Árboles_Binarios
{

    public partial class Form1 : Form
    {
        Arbol<int> arbol = new Arbol<int>();

        public Form1()
        {
            InitializeComponent();
            pbCancha.Invalidate();
        }

        

        private void buttonAgregarNodo_Click(object sender, EventArgs e)
        {
            arbol.AñadirRecursivo(int.Parse(textBox1.Text));
            pbCancha.Invalidate(); // pide que se redibuje el árbol
        }

        private void buttonBorrar_Click(object sender, EventArgs e)
        {
            lbExplicacion.Items.Add("Voy a borrar " + textBox1.Text);
            Nodo<int> borrable = arbol.Buscar(int.Parse(textBox1.Text), lbExplicacion);

            if (borrable != null)
            {
                arbol.BorrarArbol(borrable, lbExplicacion);
            }

            lbExplicacion.Items.Add("Terminé el proceso de borrar, por favor pulse 'mostrar resultado' para ver cómo quedó el árbol");
            // pbCancha.Invalidate(); // pide que se redibuje el árbol, pero usaremos el boton de mostrar resultado
        }

        private void pbCancha_Paint_1(object sender, PaintEventArgs e)
        {
            arbol.Dibujar(e.Graphics);
        }

        private void bMostrarResultado_Click(object sender, EventArgs e)
        {
            pbCancha.Invalidate();
        }
    }
}
