﻿namespace Árboles_Binarios
{
    partial class Form1
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.pbCancha = new System.Windows.Forms.PictureBox();
            this.buttonAgregarNodo = new System.Windows.Forms.Button();
            this.buttonBorrar = new System.Windows.Forms.Button();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.lbExplicacion = new System.Windows.Forms.ListBox();
            this.bMostrarResultado = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.pbCancha)).BeginInit();
            this.SuspendLayout();
            // 
            // pbCancha
            // 
            this.pbCancha.BackColor = System.Drawing.Color.White;
            this.pbCancha.Location = new System.Drawing.Point(24, 28);
            this.pbCancha.Margin = new System.Windows.Forms.Padding(2);
            this.pbCancha.Name = "pbCancha";
            this.pbCancha.Size = new System.Drawing.Size(540, 269);
            this.pbCancha.TabIndex = 0;
            this.pbCancha.TabStop = false;
            this.pbCancha.Paint += new System.Windows.Forms.PaintEventHandler(this.pbCancha_Paint_1);
            // 
            // buttonAgregarNodo
            // 
            this.buttonAgregarNodo.Location = new System.Drawing.Point(356, 323);
            this.buttonAgregarNodo.Margin = new System.Windows.Forms.Padding(2);
            this.buttonAgregarNodo.Name = "buttonAgregarNodo";
            this.buttonAgregarNodo.Size = new System.Drawing.Size(80, 30);
            this.buttonAgregarNodo.TabIndex = 1;
            this.buttonAgregarNodo.Text = "Agregar Nodo";
            this.buttonAgregarNodo.UseVisualStyleBackColor = true;
            this.buttonAgregarNodo.Click += new System.EventHandler(this.buttonAgregarNodo_Click);
            // 
            // buttonBorrar
            // 
            this.buttonBorrar.Location = new System.Drawing.Point(477, 323);
            this.buttonBorrar.Margin = new System.Windows.Forms.Padding(2);
            this.buttonBorrar.Name = "buttonBorrar";
            this.buttonBorrar.Size = new System.Drawing.Size(76, 26);
            this.buttonBorrar.TabIndex = 2;
            this.buttonBorrar.Text = "Borrar Nodo";
            this.buttonBorrar.UseVisualStyleBackColor = true;
            this.buttonBorrar.Click += new System.EventHandler(this.buttonBorrar_Click);
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(223, 331);
            this.textBox1.Margin = new System.Windows.Forms.Padding(2);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(78, 20);
            this.textBox1.TabIndex = 3;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(230, 303);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(36, 13);
            this.label1.TabIndex = 4;
            this.label1.Text = "Nodo:";
            // 
            // lbExplicacion
            // 
            this.lbExplicacion.FormattingEnabled = true;
            this.lbExplicacion.Location = new System.Drawing.Point(569, 28);
            this.lbExplicacion.Name = "lbExplicacion";
            this.lbExplicacion.Size = new System.Drawing.Size(513, 264);
            this.lbExplicacion.TabIndex = 5;
            // 
            // bMostrarResultado
            // 
            this.bMostrarResultado.Location = new System.Drawing.Point(762, 323);
            this.bMostrarResultado.Name = "bMostrarResultado";
            this.bMostrarResultado.Size = new System.Drawing.Size(152, 23);
            this.bMostrarResultado.TabIndex = 6;
            this.bMostrarResultado.Text = "Mostrar resultado";
            this.bMostrarResultado.UseVisualStyleBackColor = true;
            this.bMostrarResultado.Click += new System.EventHandler(this.bMostrarResultado_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1094, 366);
            this.Controls.Add(this.bMostrarResultado);
            this.Controls.Add(this.lbExplicacion);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.buttonBorrar);
            this.Controls.Add(this.buttonAgregarNodo);
            this.Controls.Add(this.pbCancha);
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "Form1";
            this.Text = "Form1";
            ((System.ComponentModel.ISupportInitialize)(this.pbCancha)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pbCancha;
        private System.Windows.Forms.Button buttonAgregarNodo;
        private System.Windows.Forms.Button buttonBorrar;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ListBox lbExplicacion;
        private System.Windows.Forms.Button bMostrarResultado;
    }
}

