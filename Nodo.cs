﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;

namespace Árboles_Binarios
{
    class Nodo<T>
    {
        public T Value { get; set; } //Propiedad valor y propiedad Next
        public Nodo  <T> Left { get; set; } //Propiedad, llamarse a si mismo, recursividad
        public Nodo <T> Right { get; set; }
        public Nodo <T> Padre { get; set; }

        private int Tamano;
        

        public Nodo(T Value)
        {
            this.Value = Value;
            this.Left = null;
            this.Right = null;//Sgte segmento de memoria no existe, por eso es null
            this.Padre = null;

            Tamano = 30;
        }
        // override o invalidaciones
        public override string ToString() //Sobrecarga del ToString
        {
            return String.Format("[{0}]", Value); //Cuando llame a imprimir, va a mostrar value entre corchetes
        }

        public void Dibujar(Graphics graphics, int centroX, int cimaY)
        {
            graphics.DrawRectangle(new Pen(Color.Black, 2), new Rectangle(centroX, cimaY, Tamano, Tamano));       
            graphics.DrawString(Value.ToString(), new Font(FontFamily.GenericSansSerif, 12), Brushes.Black, new RectangleF(centroX, cimaY, Tamano, Tamano));
        }
    }
}
